<?php

namespace Database\Factories;

use App\Models\CarAd;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarAdFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CarAd::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'price' => rand(200, 125000),
            'year' => rand(1995, 2021),
            'capacity' => rand(800, 5000),
            'engine_type' => rand(0, 1) == 1 ? "Pb" : "De",
            'car_id' => rand(1, 4)
        ];
    }
}
