<?php

use App\Models\Car;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('country');
            $table->timestamps();
        });

        Car::create([
            'name' => 'Opel',
            'country' => 'DE'
        ]);
        Car::create([
            'name' => 'BMW',
            'country' => 'DE'
        ]);
        Car::create([
            'name' => 'Peugeot',
            'country' => 'FR'
        ]);
        Car::create([
            'name' => 'Toyota',
            'country' => 'JP'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
