<?php

namespace App\Services;

use App\Models\CarAd;

class CarAdService
{
    private $ads;

    public function __construct()
    {
        $this->ads = CarAd::all();
        $this->fullAds = CarAd::with('car')->get();        
    }

    public function getAveragePrice()
    {
        $average = from($this->ads)->average(function ($ads) { return $ads['price']; });

        return $average;
    }

    public function getMostExpensiveAds($howManyToSkip = 10, $percentage = 20)
    {
        $average = from($this->ads)->orderByDescending(function ($ads) { return $ads['price']; })
            ->skip($howManyToSkip)
            ->take(floor(count($this->ads) * $percentage / 100))
            ->average(function ($ads) { return $ads['price']; });

        return $average;
    }

    public function countAdsByEngineType($type = 'Pb')
    {
        $count = from($this->ads)->where(function ($ads) use ($type) { return $ads['engine_type'] == $type; })
            ->count();

        return $count;
    }

    public function countAds($year, $minPrice = 200, $maxPrice = 125000)
    {
        $count = from($this->ads)->where(function ($ads) use ($year, $minPrice, $maxPrice) { 
            return $ads['year'] == $year && $ads['price'] > $minPrice && $ads['price'] < $maxPrice;
        })->count();

        return $count;
    }

    public function countBasedOnCapacity($country = 'DE', $minCapacity = 2000)
    {
        $count = from($this->fullAds)->where(function ($ads) use ($country, $minCapacity) {
            return $ads['capacity'] > $minCapacity && $ads->car->country == $country; 
        })->count();

        return $count;
    }

    public function findExample($minYear, $minCapacity, $make, $engineType)
    {
        $ad = from($this->fullAds)->where(function ($ads) use ($minYear, $minCapacity, $make, $engineType) {
            return $ads['capacity'] > $minCapacity && 
                $ads->car->name == $make && 
                $ads->year > $minYear &&
                $ads['engine_type'] == $engineType;
        })->orderByDescending(function ($ads) { return $ads['price']; })
        ->firstOrDefault();
        
        return $ad;
    }
}