<?php

namespace App\Http\Controllers;

use App\Services\CarAdService;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    public function index(CarAdService $carAdService)
    {
        $average = $carAdService->getAveragePrice();
        $topAverage = $carAdService->getMostExpensiveAds(1000);
        $engineCount = $carAdService->countAdsByEngineType();
        $yearCount = $carAdService->countAds(2020, 70000);
        $capacityCount = $carAdService->countBasedOnCapacity('FR');
        $ad = $carAdService->findExample(2010, 800, 'BMW', 'De');
        
        return view('welcome')->with(compact('average', 'topAverage', 'engineCount', 'yearCount', 'capacityCount', 'ad'));
    }
}
