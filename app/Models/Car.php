<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = ['name', 'country'];
    
    public function carAds()
    {
        return $this->hasMany('App\Models\CarAd');
    }

    use HasFactory;
}
