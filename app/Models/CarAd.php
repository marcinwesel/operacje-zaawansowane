<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarAd extends Model
{
    public function car()
    {
        return $this->belongsTo('App\Models\Car');
    }

    use HasFactory;
}
